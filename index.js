'use strict';
const express = require('express');
const expressStaticGzip = require('express-static-gzip');
const app = express();
const bodyParser = require('body-parser');
const path = require('path');
// const cors = require('cors');

const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const history = require('connect-history-api-fallback');

require('dotenv').config();

const webpackConfig = require(path.join(__dirname, 'webpack.config.js'));

const NODE_ENV = process.env.NODE_ENV || 'dev';

console.log('/*****************************/');
console.log(`/*   starting API in  ${NODE_ENV}   */`);
console.log('/*****************************/');

// app.use(cors());
app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // to support URL-encoded bodies

// middleware qui rajoute le cross origin sinon je ne peux pas tester en local
app.use((request, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  console.log(request.originalUrl);
  next();
});

// api services
// on prefixe tous les appels aux services par "api/", pas besoin de le rajouter dans les get/post dans les fichiers qui sont inclus ci-dessous
/*
const api_services = require(path.join(__dirname, 'api', 'api.js'));
app.use('/api/', api_services);
*/


// DEVELOPMENT
if (!['production', 'integration'].includes(NODE_ENV)) {
  console.log('development mode');
  console.log(`Building webpack resources...`);

  const compiler = webpack(webpackConfig);
  app.use(webpackDevMiddleware(compiler, { publicPath: webpackConfig.output.publicPath, writeToDisk: true }));
  app.use(webpackHotMiddleware(compiler));
}

let distDirectory = webpackConfig.output.path;
let distIndex = path.resolve(distDirectory, 'index.html');

app.use(
  expressStaticGzip(path.resolve(distDirectory), {
    enableBrotli: true,
    customCompressions: [
      {
        encodingName: 'deflate',
        fileExtension: 'zz',
      },
    ],
    orderPreference: ['br'],
  })
);
// app.use(express.static(path.resolve(distDirectory)));

// app.use(express.static(path.join(__dirname,  'dist')));
app.get('*', (req, res) => {
  res.sendFile(distIndex);
});

// starting the serveur
let _port = process.env.APP_PORT;
app.listen(_port, function () {
  console.log('App listening on port ' + _port);
});

module.exports = app;
