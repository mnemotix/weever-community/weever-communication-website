import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Config from '../Config';

export default function useResponsive() {
  const theme = useTheme();

  const isMobile = useMediaQuery(theme.breakpoints.down(Config.theme.breakpoint));
 
  return {
    isMobile,
    isDesktop: !isMobile,
  };
}
