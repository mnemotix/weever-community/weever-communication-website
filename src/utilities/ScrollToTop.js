import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';

/**
 * This component is a utility to make the view scroll to top on any route change.
 * See https://reacttraining.com/react-router/web/guides/scroll-restoration
 *
 * It is used on the top of the application tree
 *
 */
export default function ScrollToTop({children}) {
  let location = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [location.pathname]);

  return children;
}
