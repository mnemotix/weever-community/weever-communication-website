// editer le fichier avec boxysvg pour changer l'année
import copyright from '../public/images/copyright.svg';
import copyright2021 from '../public/images/copyright2021.svg';
import copyright2022 from '../public/images/copyright2022.svg';
import copyright2023 from '../public/images/copyright2023.svg';
import copyright2024 from '../public/images/copyright2024.svg';
import copyright2025 from '../public/images/copyright2025.svg';
import copyright2026 from '../public/images/copyright2026.svg';
import arrowdown from '../public/images/arrowdown.svg';

import home_back from '../public/images/home_back.png';
import home_back_small from '../public/images/home_back_small.png';
import icona from '../public/images/icon_a.svg';
import iconb from '../public/images/icon_b.svg';
import iconc from '../public/images/icon_c.svg';
import icond from '../public/images/icon_d.svg';
import icone from '../public/images/icon_e.svg';
import iconah from '../public/images/icon_a_h.svg';
import iconbh from '../public/images/icon_b_h.svg';
import iconch from '../public/images/icon_c_h.svg';
import icondh from '../public/images/icon_d_h.svg';
import iconeh from '../public/images/icon_e_h.svg';
import logo from '../public/images/logo.svg';
import weever from '../public/images/weever.svg';
import wwwww from '../public/images/wwwww.svg';
import ww from '../public/images/ww.svg';
import iconensemble from '../public/images/ensemble.svg';
import iconensembleh from '../public/images/ensemble_h.svg';

import whatSkos from '../public/images/pages/what-skos.gif';
import editDrupal from '../public/images/pages/editorialisation-site-drupal.png';
import lafayetteRebond from '../public/images/pages/lafayette-rebond.jpg';
import weeverEvent from '../public/images/pages/weever-event.png';
import weeverEventGif from '../public/images/pages/weever-event.gif';
import ligneProjet from '../public/images/pages/ligne-projet.png';
import workflowLuma from '../public/images/pages/workflow-luma.png';
import annotationSemantiqueRessource from '../public/images/pages/weever-screenshots/annotation-semantique-ressource.png';
import detailOrganisationMcc from '../public/images/pages/weever-screenshots/detail-organisation-mcc.png';
import listeOrganisations from '../public/images/pages/weever-screenshots/liste-organisations.png';
import memoTagSemantique from '../public/images/pages/weever-screenshots/memo-tag-semantique.png';
import rechercheFiltres from '../public/images/pages/weever-screenshots/recherche-filtres.png';
import rechercheRessourcesSiteWeever from '../public/images/pages/weever-screenshots/recherche-ressources-site-weever.png';
import ressourcesEvenement from '../public/images/pages/weever-screenshots/ressources-evenement.png';
import selectionRessourcesAvantVersementRecadre from '../public/images/pages/weever-screenshots/selection-ressources-avant-versement-recadre.png';
import selectionRessourcesAvantVersement from '../public/images/pages/weever-screenshots/selection-ressources-avant-versement.png';
import coopair from '../public/images/pages/weever-screenshots/coopair.png';
import arcadiis from '../public/images/pages/weever-screenshots/arcadiis.jpg';
import thesoPactols from '../public/images/pages/weever-screenshots/thesopactols.jpg';


const ImportedGraphics = {
  copyright,
  copyright2021,
  copyright2022,
  copyright2023,
  copyright2024,
  copyright2025,
  copyright2026,
  iconensemble,
  iconensembleh,
  icona,
  iconb,
  iconc,
  icond,
  icone,
  iconah,
  iconbh,
  iconch,
  icondh,
  iconeh,
  logo,
  weever,
  wwwww,
  ww,
  arrowdown, whatSkos, editDrupal, lafayetteRebond, weeverEvent, weeverEventGif, ligneProjet, workflowLuma,
  annotationSemantiqueRessource, detailOrganisationMcc, listeOrganisations, memoTagSemantique,
  rechercheFiltres, rechercheRessourcesSiteWeever, ressourcesEvenement, selectionRessourcesAvantVersement,
  selectionRessourcesAvantVersementRecadre, coopair, arcadiis, thesoPactols,
  home_back, home_back_small
};
export default ImportedGraphics;
