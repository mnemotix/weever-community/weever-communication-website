import React from 'react';
import globalStyles from '../globalStyles';
import clsx from 'clsx';
import Container from '@material-ui/core/Container';
import useMediaQueries from '../utilities/useResponsive';
/**
 * permet de mettre une largeur max et de centrer du contenu
 *
 */
export default function MaxWidthCentered({ size = 'lg', children, ...props }) {
  const gS = globalStyles();
  const {isDesktop} = useMediaQueries();
  return (
    <Container maxWidth={size} {...props} className={gS.noPaddingLR}>
      <div className={clsx(gS.flexCenter, gS.flexDirectionCol,isDesktop ? gS.p20 : gS.p10 )}>{children}</div>
    </Container>
  );
}
