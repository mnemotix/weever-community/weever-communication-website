import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import globalStyles from '../globalStyles';
import Config from '../Config';
import clsx from 'clsx';
import {menuEntries} from "../pages/Home/IconEntries";

const useStyles = makeStyles((theme) => ({
  container: {
    width: '100%',
    flex: 1,
    margin: `${Config.logoTop * 2}px 0`,
    [theme.breakpoints.down('sm')]: {
      margin: Config.logoTop,
    },
    padding: `${Config.logoTop}px 0`,
  },
  marginB20: {
    marginBottom: '20px'
  },
  title: {
    fontSize: '2.4rem',
    fontFamily: 'LatoBlack, sans-serif',
    fontWeight: 'bold',
    [theme.breakpoints.down('sm')]: {
      fontSize: 'calc(25px + 2vmin)',
    }
  },
  subTitle: {
    marginTop: '8px',
    fontSize: '2rem',
  },
  content: {
    fontSize: '1.8rem',
    [theme.breakpoints.down('lg')]: {
      fontSize: '1.6rem',
    },
    textAlign: "justify",
    textJustify: "inter-word"
  },
  entrie: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  entrieLogo: {
    marginRight: '15px',
    height: `min(5vw,100px)`
  },

}));

export default function PageContainer({title, children}) {
  const classes = useStyles();
  const gS = globalStyles();

  // if we have a matching entry we display icon and subTitle to the title
  const entrie = menuEntries.find(entry => entry?.href === location.pathname);

  function renderTitle() {
    if (entrie?.useCaseTitle) {
      return (
        <div className={classes.entrie}>
          <img src={entrie.src} className={classes.entrieLogo} />
          <div className={classes.title}>
            {entrie.useCaseTitle}
            <div className={classes.subTitle} style={{color: entrie.color}}>
              {entrie.useCaseSubTitle}
            </div>
          </div>
        </div>
      );
    } else {
      return <div className={clsx(classes.title, classes.marginB20)}>{title}</div>
    }
  }
  return (
    <div className={classes.container}>
      {renderTitle()}
      <div className={classes.content}>{children}</div>
    </div>
  );
}
