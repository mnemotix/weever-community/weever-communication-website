import React, {useState, useEffect} from 'react';
import {createMuiTheme, ThemeProvider, makeStyles} from '@material-ui/core/styles';
import {AppBar, Toolbar, IconButton, Typography, MenuItem, Menu} from '@material-ui/core';

import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import MaxWidthCentered from './MaxWidthCentered';
import FlexboxWrap from './FlexboxWrap';
import MenuIcon from '@material-ui/icons/Menu';
import clsx from 'clsx';
import logoBig from '../../public/images/logo-headerBig.svg';
import logo from '../../public/images/logo.svg';
import Config from '../Config';
import globalStyles from '../globalStyles';
import {menuEntries} from '../pages/Home/IconEntries.js';
import useMediaQueries from '../utilities/useResponsive';

const subMenuEntries = menuEntries.filter(entry => entry?.inmenu === true);
const theme = createMuiTheme({
  overrides: {
    MuiPopover: {
      paper: {
        width: '100% !important',
        maxWidth: '100% !important',
        height: '80%',
        maxHeight: 'unset',
        left: '0% !important',
      },
    },
  },
});

const useStyles = makeStyles((theme) => ({
  appbar: {
    height: Config.navbar.height,
    alignItems: 'center',
    backgroundColor: 'white',
    color: Config.colors.dark,
    backgroundColor: Config.colors.grey,
    display: 'flex',
    justifyContent: 'center',
  },
  smallAppbar: {
    height: `${Config.navbar.height / 1.5}px`,
    transition: 'all 400ms ease-out',
  },
  logoContainer: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  logo: {
    height: `${Config.navbar.height / 1.2}px`,
    maxHeight: `${Config.navbar.logoMaxHeight}px`,
    maxWidth: '65vw',
    margin: 'auto'
  },
  smallLogo: {
    // width: 'min(50vw, 250px)',
    height: 'min(9vw, 66px)'
  },

  menuIcon: {
    width: '10vw',
    height: '10vw',
    padding: '2vw 1vh',
    maxHeight: `${Config.navbar.height / 2}px`,
    maxWidth: `${Config.navbar.height / 2}px`,
  },

  menuButton: {
    marginRight: theme.spacing(2),
  },

  ahref: {
    listStyle: 'none',
    fontSize: '15px',
    lineHeight: '20px',
    fontWeight: '700',
    textTransform: 'uppercase',
    textDecoration: 'none',
    border: '1px solid rgba(255, 255, 255, 0)',
    borderRadius: '3px',
    zIndex: '1',
    color: Config.colors.dark,
  },

  sectionDesktop: {
    display: 'flex',
    //flex: 1,
    marginLeft: '15px'
  },
  sectionMobile: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  m5: {
    margin: '5px !important',
  },
  m10: {
    margin: '10px !important',
  },
  flexCenter: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    //flex: 1,
  },
  flexLeft: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  entrieLogo: {
    height: `min(5vw,32px)`,
    width: 'auto',
    padding: '0px 10px',
    [theme.breakpoints.down(Config.theme.breakpoint)]: {
      height: `max(4w,25px)`
    },
  },
  nopd: {
    margin: 0,
    padding: 0,
    position: "relative",
    listStyle: "none"
  }
}));



const entries = [
  {
    href: "/concept",
    menuTitle: "Concept"
  }, {
    menuTitle: "Cas d'usages",
    subMenu: subMenuEntries
  }, {
    href: "/roadmap",
    menuTitle: "Roadmap"
  }, {
    target: {target: "_blank"},
    href: "https://gitlab.com/groups/mnemotix/weever-community/-/issues",
    menuTitle: "Support"
  }, {
    href: "mailto:contact@mnemotix.com",
    menuTitle: "Contact"
  }
];


/**
 * header avec la barre de navigation
 */
export default function NavHeader() {

  const {isDesktop} = useMediaQueries();
  const gS = globalStyles();
  const classes = useStyles();
  // le menu en mode mobile
  const [mobileAnchorEl, setMobileAnchorEl] = useState(null);
  // pour ouvrir ou ferme le sous menu cas d'usage en mode desktop
  const [desktopSubAnchorEl, setDesktopSubAnchorEl] = useState(null);

  const handleMobileMenuClose = () => {
    setMobileAnchorEl(null);
  };
  const handleMobileMenuOpen = (event) => {
    setMobileAnchorEl(event.currentTarget);
  };
  const handleDesktopSubOpen = (event) => {
    setDesktopSubAnchorEl(event.currentTarget);
  };
  const handleDesktopSubClose = () => {
    setDesktopSubAnchorEl(null);
  };

  useEffect(() => {
    handleDesktopSubClose();
    handleMobileMenuClose();
  }, [isDesktop]);



  // au scroll dans la page on reduit la taille du logo principal et la hauteur du header
  const showSmallHeader = useScrollTrigger();

  /**
   * createEntry, can be recursive call
   * isSub is to remove padding for sub menu
   */
  function createEntry({href, menuTitle, target, subMenu, src}, keyindex, isSub = false) {
    if (subMenu) {
      // if desktop no need to display sub because they are displayed in a dedicated sub menu
      if (isDesktop) {
        // affiche uniquement le bouton cas d'usage 
        return <div className={classes.m5} key={keyindex}>
          <Typography variant="h6" noWrap>
            <a aria-label="sous-menu"
              aria-controls={desktopSubMenuId}
              aria-haspopup="true"
              onClick={handleDesktopSubOpen}
              className={clsx(!isSub && gS.p10, gS.pointerCursor, gS.ahref, gS.latoBlack, gS.font19, location.pathname === href && gS.selected)}> {menuTitle}</ a>
          </Typography>
        </div>
      } else {
        // in mobile we add them 
        return <div className={classes.m5} key={keyindex}>
          <a className={clsx(!isSub && gS.p10, gS.latoBlack, gS.font19, location.pathname === href && gS.selected)}> {menuTitle}</ a>
          <ul className={classes.nopd}>
            {subMenu.map((item, index) => (
              <MenuItem key={index} onClick={handleMobileMenuClose}>
                {createEntry(item, "submobile" + index, true)}
              </MenuItem>
            ))}
          </ul>
        </div>
      }
    }
    return (<div className={classes.m5} key={keyindex}>
      <Typography variant="h6" noWrap>
        <a href={href} {...target} className={clsx(!isSub && gS.p10, gS.ahref, gS.latoBlack, gS.font19, classes.flexLeft, location.pathname === href && gS.selected)}>
          {src ? (
            <>
              <img src={src} className={classes.entrieLogo} />
              {menuTitle}
            </>
          ) : menuTitle}
        </ a>
      </Typography>
    </div>
    )
  }

  const renderDesktopSubMenu = (
    <Menu
      id={desktopSubMenuId}
      anchorEl={desktopSubAnchorEl}
      keepMounted
      open={Boolean(desktopSubAnchorEl)}
      onClose={handleDesktopSubClose}
      getContentAnchorEl={null}
      anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
      transformOrigin={{vertical: 'top', horizontal: 'center'}}
    >
      {subMenuEntries.map((item, index) => (
        <MenuItem key={index} onClick={handleDesktopSubClose}>
          {createEntry(item, 'desktop', true)}
        </MenuItem>
      ))}
    </Menu>
  );




  const mobileSubMenuId = 'mobile-sub-menu-id';
  const desktopSubMenuId = "desktop-Sub-Menu-Id";
  // affiche le menu en mode mobile uniquement via popover
  const renderMobileMenu = (
    <ThemeProvider theme={theme}>
      <Menu
        anchorReference="anchorPosition"
        anchorPosition={{top: showSmallHeader ? Config.navbar.height / 1.5 : Config.navbar.height, left: 0}}
        id={mobileSubMenuId}
        keepMounted
        anchorOrigin={{vertical: 'bottom', horizontal: 'left'}}
        transformOrigin={{vertical: 'top', horizontal: 'left'}}
        open={Boolean(mobileAnchorEl)}
        onClose={handleMobileMenuClose}
      >
        {entries.map((item, index) => (
          <MenuItem key={index} onClick={handleMobileMenuClose}>
            {createEntry(item, "mobile" + index)}
          </MenuItem>
        ))}
      </Menu>
    </ThemeProvider>
  );

  return (
    <>
      <AppBar position="fixed" className={clsx(classes.appbar, showSmallHeader && classes.smallAppbar)}>
        <MaxWidthCentered size='xl'>
          <Toolbar disableGutters={true} className={gS.flexSpaceBetween}>
            <a className={classes.logoContainer} href="/">
              <img alt="Mnemotix" src={showSmallHeader ? logo : logoBig} className={clsx(classes.logo, showSmallHeader && classes.smallLogo)} />
            </a>
            {isDesktop ?
              <div className={classes.sectionDesktop}>
                <FlexboxWrap>
                  {entries.map((item, index) => createEntry(item, "desktop" + index))}
                </FlexboxWrap>
              </div>
              :
              <div className={classes.sectionMobile}>
                <IconButton
                  className={classes.largeIcon}
                  aria-label="show more"
                  aria-controls={mobileSubMenuId}
                  aria-haspopup="true"
                  onClick={handleMobileMenuOpen}
                  color="inherit"
                >
                  <MenuIcon className={classes.menuIcon} />
                </IconButton>
              </div>
            }
          </Toolbar>
        </MaxWidthCentered>
      </AppBar>
      {renderMobileMenu}
      {renderDesktopSubMenu}
    </>
  );
}
