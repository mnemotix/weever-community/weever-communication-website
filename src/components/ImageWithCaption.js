import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
	root: {
		flex: 1,
		margin: '25px 0px 15px 0px',
		padding: '20px'
	},
	media: {
		width: "100%",
		height: 'auto',
	},
	text: {
		marginTop: '10px',
		color: "rgba(0, 0, 0, 0.54)",
		fontSize: '1.7rem',
		textAlign: 'center',
	}
});

export default function ImageWithCaption({imageSrc, imageTitle, children}) {
	const classes = useStyles();

	return (
		<Paper
			className={classes.root}
			elevation={2}
		>
			<img className={classes.media}
				src={imageSrc}
				title={imageTitle}
			/>
			{children &&
				<Typography component="h5" variant="h5" className={classes.text}>
					{children}
				</Typography>
			}

		</Paper>
	)


}