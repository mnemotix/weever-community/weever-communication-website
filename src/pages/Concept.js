import React from 'react';
import ImageWithCaption from '../components/ImageWithCaption';
import PageContainer from '../components/PageContainer';
import globalStyles from '../globalStyles';
import ImportedGraphics from '../ImportedGraphics';

export default function Concept(props) {
  const gS = globalStyles();

  return (
    <PageContainer title="Qu'est-ce que Weever ?">
      <div>
        <div >
          <p>Weever est un outil innovant de documentation collaborative en “mode projet”. Il a été pensé pour permettre à des utilisateurs non-documentalistes de contribuer le plus simplement possible aux processus d’archivage et de documentation au sein des organisations (entreprises, institutions publiques, etc.).</p>
        </div>
        <div >

          Ses principales fonctionnalités sont:
          <ImageWithCaption
            imageSrc={ImportedGraphics.annotationSemantiqueRessource}
            imageTitle="annotation sémantique de ressource"
          >
            Versement et annotation de ressources média (document, fichier, images)
          </ImageWithCaption>


          <ImageWithCaption
            imageSrc={ImportedGraphics.memoTagSemantique}
            imageTitle="notes collaboratives"
          >
            Edition et publication de notes collaboratives pouvant être taguées à l’aide des concepts de la base de connaissance ou des concepts suggérés par les usagers
          </ImageWithCaption>


          <ImageWithCaption
            imageSrc={ImportedGraphics.detailOrganisationMcc}
            imageTitle="carnet d'adresses"
          >
            Gestion de carnets d’adresses, de projets, et d’acteurs liés aux organisations
          </ImageWithCaption>


          <ImageWithCaption
            imageSrc={ImportedGraphics.rechercheFiltres}
            imageTitle="recherche filtres"
          >
            Navigation et recherche de contenus par facette dans les ressources stockées
          </ImageWithCaption>

          <p>Weever, à travers des fonctions de gestion de contenu très simple, permet une captation des connaissances des organisations: projets, acteurs internes ou partenaires, domaines d’activité, et tout autre champ de connaissances sont ainsi organisés et structurés sans efforts supplémentaires. Afin de faciliter le démarrage du processus, des schémas de concepts ou vocabulaires contrôlés peuvent également être chargés initialement dans la base de données afin de suggérer aux usagers les concepts utiles et ainsi faciliter l’annotation.</p>

          <ImageWithCaption
            imageSrc={ImportedGraphics.coopair}
            imageTitle="coopair"
          />
          <p>Cette structuration selon les formats du Web Sémantique décuple les potentiels de réutilisation des données. Le coeur technique de Weever, Synaptix, est composé de modules de structuration et de gestion des données développés par Mnemotix et issus de plusieurs années de recherche dans le champ du Web Sémantique et de l'Ingénierie des connaissances. Grâce à Synaptix et aux ontologies utilisées au cœur de la structuration des données, il est possible de générer de nouvelles données par inférence, et ainsi de créer des liens entre les données qui n’étaient pas explicités au départ. Par exemple si une image est taguée avec le concept de Tour Eiffel, le système pourra en déduire que l’image traite également de monuments historiques situés à Paris.</p>
          <p> L’appropriation de Weever par une communauté permet donc à la fois de produire une documentation riche (de projet, de processus, de ressources média, etc.) et une description structurée de toutes les connaissances manipulées par les membres des organisations. Cette approche “bottom up” de la gestion des connaissances partagées permet de révéler rapidement et efficacement le patrimoine immatériel propre à chaque organisation.</p>
          <p>Weever couvre déjà de nombreux cas d’usages, et est en constante évolution. En tant que "commun logiciel", son évolution est pilotée par la communauté de ses usagers et co-financeurs. Cette gestion de la propriété intellectuelle innovante permet en retour de réduire les coûts en mutualisant les développements. </p>
        </div>
      </div>
    </PageContainer>
  );
}
