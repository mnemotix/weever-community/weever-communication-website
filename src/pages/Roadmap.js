import React from 'react';
import PageContainer from '../components/PageContainer';
import globalStyles from '../globalStyles';

export default function Roadmap(props) {
	const gS = globalStyles();

	return (
		<PageContainer >
			<div >
				<p>
					Avec la volonté de démocratiser nos technologies à la pointe de la recherche, nous proposons un modèle commercial innovant basé sur le développement de communs logiciels open source mutualisés au sein de communautés d’utilisateurs et de développeurs.
		    </p>
				Actuellement Weever compte les contributeurs/financeurs suivants :
				<ul>
					<li>Lafayette Anticipation (FEGL)</li>
					<li>Atelier Luma</li>
					<li>projet ArcaDIIS (Chercheurs en archéologie, CNRS, Aix en Provence)</li>
					<li>laboratoire Origens Média Lab (Clermont-F)</li>
				</ul>

				<p> Vous pensez que Weever répond à vos besoins ?Vous auriez besoin d’une fonctionnalité supplémentaire et aimerez mutualiser son développement avec les autres contributeurs ? Contactez-nous et rejoignez la communauté Weever!
				</p>
			</div>
		</PageContainer>
	);
}
