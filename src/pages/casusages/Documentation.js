import React from 'react';
import PageContainer from '../../components/PageContainer';
import ImageWithCaption from '../../components/ImageWithCaption';
import globalStyles from '../../globalStyles';
import ImportedGraphics from '../../ImportedGraphics';

export default function Documentation(props) {
  const gS = globalStyles();

  return (
    <PageContainer>
      <div>
        <p>
          <b> Contexte: </b>
          Weever a été adopté par l’UMR <a target="_blank" href="https://lampea.cnrs.fr/">7269-LaMPEA</a> pour le <a target="_blank" href="https://www.univ-amu.fr/fr/public/arcadiis">projet ArcaDIIS</a>{" "} 
          pour valoriser les données issues de la recherche en archéologie ainsi que les étapes de l’analyse scientifique. Elle repose sur l’enrichissement sémantique et qualitatif des données.
        </p>

        <ImageWithCaption
          imageSrc={ImportedGraphics.arcadiis}
          imageTitle="projet arcadiss"
        >
          Documentation de projets archéologiques par les membres du consortium ArcaDIIS
        </ImageWithCaption>


        <p>
          <b>Description:</b> Beaucoup d'organisations sont confrontées au problème de la perte d’information lors des projets de production. Le grand volume de données généré pendant un projet (images, documents,  vidéos…) sont riches d’enseignement sur la vie du projet mais bien souvent éparpillées, et rarement exploitées.
        </p>

        Nous avons donc créé Weever pour permettre à des utilisateurs non-documentalistes de contribuer le plus simplement possible au processus d’archivage et de documentation de leurs  projets. Pour rendre accessible ce processus de documentation, Weever combine :
        <ul>
          <li>Stockage collaboratif (type Dropbox, Google Drive, iCloud) </li>
          <li>Prises de note (type Evernote) </li>
          <li>Annotations sémantiques automatiques de manière à limiter au maximum le travail d’indexation.</li>
        </ul>

        <ImageWithCaption
          imageSrc={ImportedGraphics.thesoPactols}
          imageTitle="projet arcadiss"
        >
          Annotation de contenu à l'aide des concepts du thesaurus Pactols
        </ImageWithCaption>


        <p>
          Les contenus versés par les utilisateurs sont ensuite analysés puis annotés sémantiquement, pour enfin être agrégés dans des lignes de temps correspondants aux différents projets de manière à établir une chronologie, la plus précise et la plus exhaustive possible, du déroulement du projet.
          </p>

      </div>
    </PageContainer>
  );
}
