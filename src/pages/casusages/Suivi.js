import React from 'react';
import PageContainer from '../../components/PageContainer';
import ImageWithCaption from '../../components/ImageWithCaption';
import ImportedGraphics from '../../ImportedGraphics';
import globalStyles from '../../globalStyles';

export default function Suivi(props) {
  const gS = globalStyles();

  return (
    <PageContainer>
      <div >
        <p><b> Contexte: </b>
          Weever a été adopté par Lafayette Anticipations pour documenter le processus ou l’enquête collective qui conduit à la production des œuvres d’art. Utilisé par le laboratoire de recherche en archéologie le LAMPEA pour permettre le travail collaboratif autour des ressources produites par tout projet de recherche afin de les organiser, les structurer et les enrichir.
        </p>

        <ImageWithCaption
          imageSrc={ImportedGraphics.ligneProjet}
          imageTitle="ligne de Projet"
        >
          Déroulement type d’un projet de production et diffusion d’une œuvre au sein de la Fondation Galerie Lafayette (FEGL).
        </ImageWithCaption>


        <p><b>Description:</b> Weever est une application d'un genre nouveau pour documenter le processus collectif qui  conduit à la production de projet type œuvres d’art, enquêtes SHS, projet de recherche, etc. Tout à la fois logiciel de gestion de projets, de documentation, d'archive vivante et de communication, cette chaîne applicative propose de mettre en valeur la communauté d'acteurs et de compétences qui participent à la production d’un projet. Dans Weever, un projet est défini par un ensemble  de descripteurs (date de début, date de fin, titre, acteurs impliqués, etc.) qui peuvent être tirés d’objets de la base de connaissance, et  un ensemble d’événements, </p>

        <ImageWithCaption
          imageSrc={ImportedGraphics.weeverEventGif}
          imageTitle="weever event"
        />

        <p>Les évènements quant à eux sont des objets riches qui vont servir de support à la documentation du processus ou projet. Il est ainsi possible d’associer à un évènement des informations complémentaires comme par exemple, des ressources média (texte, documents, photos, vidéos, etc.), des contacts de toute sorte (personnes rencontrées, artistes, entreprises, etc.), et de taguer ces événements à l’aide des “concepts” issus de thésaurus Skos, ou simplement suggérés par les usagers.</p>
      </div>
    </PageContainer>
  );
}
