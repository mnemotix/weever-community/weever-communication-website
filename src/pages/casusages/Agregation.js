import React from 'react';
import PageContainer from '../../components/PageContainer';
import ImageWithCaption from '../../components/ImageWithCaption';
import ImportedGraphics from '../../ImportedGraphics';
import globalStyles from '../../globalStyles';

export default function Agregation(props) {
  const gS = globalStyles();

  return (
    <PageContainer>
      <div>
        <p>
          <b> Contexte: </b>
          Weever a été adopté par Atelier Luma afin de consolider sa vocation de plateforme de connaissances à travers le prototypage d’un centre de ressources. Weever y est conçu comme un outil d’agrégation et exploration compréhensive et augmentée des différentes ressources de l’atelier (documents d’archives, matériaux, réseaux, livres, individus…) à travers plusieurs types : les archives, les ouvrages de la bibliothèque, les échantillons de matériaux ainsi que les machines / outils techniques.
          </p>
        <ImageWithCaption
          imageSrc={ImportedGraphics.rechercheRessourcesSiteWeever}
          imageTitle="workflow Luma"
        >
          Exploration de ressources annotées dans Weever
        </ImageWithCaption>

      </div>
    </PageContainer>
  );
}
