import React from 'react';
import PageContainer from '../../components/PageContainer';
import ImportedGraphics from '../../ImportedGraphics';
import globalStyles from '../../globalStyles';

export default function Editorialisation(props) {
  const gS = globalStyles();
  
  return <PageContainer>
    <div>
      <p>
        <b> Contexte: </b>
        Weever a été adopté par Lafayette Anticipations pour proposer à ses visiteurs des outils innovants de navigation et de consultation des documents de production afférents à une œuvre.
      </p>
      <img alt="éditorialisation site drupal" src={ImportedGraphics.editDrupal}  className={gS.maxWidthImg}/>
      <p>
        <b>Description:</b> Weever propose, dans les standards du web sémantique,
        plusieurs solutions d’export “ré-éditorialisé” des éléments qualifiés de Public lors de leur indexation,
        l’objectif étant de rendre accessible au plus grand nombre le résultat du travail de documentation et
        d’indexation. Weever nourrit le site public de Lafayette Anticipation, réalisé avec le CMS Drupal.
        Les ressources documentées de Weever  servent également à alimenter le
        <a href="https://www.lafayetteanticipations.com/fr/app-rebond" target="_blank"> dispositif de médiation Rebond</a> utilisé par
        les visiteurs qui viennent visiter les œuvres exposées dans leur surface d’exposition en plein cœur du Marais à Paris.
      </p>
      <img alt="éditorialisation site drupal" src={ImportedGraphics.lafayetteRebond}  className={gS.maxWidthImg}/>
    </div>
  </PageContainer>;
}
