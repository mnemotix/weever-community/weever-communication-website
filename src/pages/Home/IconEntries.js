import React, {useState} from 'react';
import {Grid} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import clsx from 'clsx';
import ImportedGraphics from '../../ImportedGraphics';
import Config from '../../Config';
import {UnstyledLink} from "../../components/widgets/UnstyledLink";

const useStyles = makeStyles((theme) => ({
  entrieContainer: {
    [theme.breakpoints.between(Config.theme.breakpoint, 'lg')]: {
      height: 'max(10vmax,160px)'
    },
    [theme.breakpoints.up("lg")]: {
      height: 'max(12vmax,225px)'     
    }
  },
  entrie: {
    cursor: 'pointer',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  entrieLogo: {
    cursor: 'pointer',
    // height: `min(10vw,150px)`,
    height: `min(7vw,125px)`,
    width: 'auto',
    [theme.breakpoints.down(Config.theme.breakpoint)]: {
      height: `max(15vw,75px)`,
      padding: '15px 0px'
    },
    [theme.breakpoints.between(Config.theme.breakpoint, 'lg')]: {
      height: `min(12vw,75px)`
    },
    [theme.breakpoints.up("lg")]: {     
      height: `min(12vw,100px)`
    }
  },
  entrieText: {
    cursor: 'pointer',
    fontFamily: 'LatoBlack, sans-serif',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    fontSize: '2.2rem', // 'calc(10px + 2vmin)',
    paddingLeft: '15px',
    [theme.breakpoints.between(Config.theme.breakpoint, 'lg')]: {
      fontSize: '2rem',
    },
    [theme.breakpoints.down(Config.theme.breakpoint)]: {
      fontSize: '1.6rem',
    },
  },
  alignTop: {
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      display: 'flex',
      justifyContent: 'flex-start',
      alignItems: 'flex-start',
      flexDirection: 'column',
    },
  },
  alignBottom: {
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'flex-start',
      flexDirection: 'column',
    },
  },
}));

/**
 * this object is used to display 3 things in
 * sub menu , home page, "cas d'usage" pages title and subtitle
 * {
    menuTitle:  title to display in the menu only
    homeIconText: text to display in home page next to icon, 
    useCaseTitle : text to display in "cas d'usage" as main title
    useCaseSubTitle : text diplayed in "cas d'usage" as sub text
    href
    src: icon displayed in home page and in "cas d'usage" pages
    srcHover: icon displayed on hover in home page  
    color: '#1C97C3', homeIconText's text's color
    inmenu : display this in menu or not
 * }
 */
export const menuEntries = [
  {
    homeIconText: (
      <>
        suivi&nbsp;de&nbsp;projet
        et&nbsp;de&nbsp;processus
      </>
    ),
    menuTitle: "Suivi de projet / processus",
    useCaseTitle: 'Suivi de projet et de processus',
    useCaseSubTitle: (
      <>
        Documentez l'intégralité de vos projets et gagnez en visibilité
      </>
    ),
    href: '/suivi',
    src: ImportedGraphics.icona,
    srcHover: ImportedGraphics.iconah,
    color: '#1C97C3',
    inmenu: true
  },
  {
    // documentation&nbsp;&nbsp;&nbsp; = pour avoir la meme longueur que éditorialisation pour que flex-end align pareil
    homeIconText: (
      <>
        documentation&nbsp;&nbsp;&nbsp;
        et&nbsp;archivage
      </>
    ),
    menuTitle: "Documentation / Archivage",
    useCaseTitle: 'Documentation et archivage',
    useCaseSubTitle: (
      <>Archivez facilement et en temps réel toute la chronologie du déroulement de votre projet</>
    ),
    href: '/documentation',
    src: ImportedGraphics.iconb,
    srcHover: ImportedGraphics.iconbh,
    color: '#8D3750',
    inmenu: true
  },

  {
    homeIconText: (
      <>
        agrégation
        et&nbsp;exploration
        de&nbsp;ressources
        documentaires
      </>
    ),
    menuTitle: "Aggrégation de documents",
    useCaseTitle: 'Agrégation et exploration de ressources documentaires',
    useCaseSubTitle: (
      <>
        Structurer et documenter facilement vos données et documents
      </>
    ),
    href: '/agregation',
    src: ImportedGraphics.iconc,
    srcHover: ImportedGraphics.iconch,
    color: '#FA8823',
    inmenu: true
  },
  {
    homeIconText: (
      <>
        éditorialisation
        de&nbsp;contenus
      </>
    ),
    menuTitle: "Éditorialisation de contenus",
    useCaseTitle: 'Éditorialisation de contenus',
    useCaseSubTitle: (
      <>
        Valorisez les connaissances et les contenus de votre organisation
      </>
    ),
    href: '/editorialisation',
    src: ImportedGraphics.icond,
    srcHover: ImportedGraphics.icondh,
    color: '#20A7AF',
    inmenu: true
  },

  {
    homeIconText: (
      <>
        Développons ensemble le futur de Weever
      </>
    ),
    useCaseTitle: "Feuille de route contributive: ensemble, développons le futur de Weever",
    useCaseSubTitle: (
      <>

      </>
    ),
    href: '/roadmap',
    src: ImportedGraphics.iconensemble,
    srcHover: ImportedGraphics.iconensembleh,
    color: '#f7dd4d',
    inmenu: false
  },
  {
    homeIconText: (
      <>
        En savoir plus
      </>
    ),
    useCaseTitle: "Qu'est-ce que Weever ?",
    useCaseSubTitle: (
      <>
        Principales fonctionnalités
      </>
    ),
    href: '/concept',
    src: ImportedGraphics.icone,
    srcHover: ImportedGraphics.iconeh,
    color: '#a6d865',
    inmenu: false
  },
];

// affiche une des 4 icons avec le texte
function renderEntrie(entrie) {
  const classes = useStyles();
  const [isShown, setIsShown] = useState(false);

  return (
    <UnstyledLink to={entrie.href} className={classes.entrie}
      onMouseEnter={() => setIsShown(true)}
      onMouseLeave={() => setIsShown(false)} >
      <>
        <img
          src={isShown ? entrie.srcHover : entrie.src}
          className={classes.entrieLogo} />
        <div className={classes.entrieText} style={{color: /*isShown ? Config.colors.dark : */ entrie.color}}>
          {entrie.homeIconText}
        </div>
      </>
    </UnstyledLink >
  );
}


export function IconEntries(props) {
  const classes = useStyles();

  let gridsContainer = [];
  let c = 0;
  for (let r = 0; r < menuEntries.length / 2; r++) {

    gridsContainer.push(
      <Grid container key={r} direction="row" justify="space-around" alignItems="flex-start">
        <Grid item xs={12} sm={6} className={clsx(classes.entrieContainer, classes.alignTop)}>
          {renderEntrie(menuEntries[c++])}
        </Grid>
        <Grid item xs={12} sm={6} className={clsx(classes.entrieContainer, classes.alignBottom)}>
          {menuEntries[c] && renderEntrie(menuEntries[c++])}
        </Grid>
      </Grid>

    );
  }
  return <>{gridsContainer}</>


}
