import React, {useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import YoutubeVideo from '../../components/YoutubeVideo';
import {IconEntries} from './IconEntries'; 
import Config from '../../Config';

const useStyles = makeStyles((theme) => ({
  spacer: {
    paddingTop: Config.logoTop,
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      paddingTop: Config.logoTop * 3
    }
  },
  alignCenter: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'column',
    flex: 1,
    width: '100%',
    marginBottom: "50px"
  },
  logoContainer: {
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      paddingLeft: Config.logoTop * 2,
    },
  },
  logo: {
    width: `min(70vw,560px)`,
    height: 'auto',
  },
  mainText: {
    fontSize: '4rem',
    fontFamily: 'Lato, sans-serif',
    fontWeight: 'bold',
    paddingTop: '6px',
    width: '100%',
    textAlign: 'left',
    [theme.breakpoints.down('sm')]: {
      fontSize: 'calc(10px + 2vmin)',
      paddingLeft: '12px',
    },

    textAlign: "center",
    textJustify: "inter-word"
  },
  videoContainer: {
    overflow: 'hidden',
    width: '100%',
    height: '0', // crée une hauteur pour la video
    paddingBottom: '35%', // crée une hauteur pour la video
    [theme.breakpoints.down('sm')]: {
      width: '90%',
      paddingBottom: '55%',
    },
    [theme.breakpoints.up('md')]: {
      height: '215px',
    },
    position: 'relative',
    backgroundColor: Config.colors.blue,
  }
}));

export default function Home() {
  const classes = useStyles();
  useEffect(() => {
    document.title = 'Weever';
  }, []);

  return (
    <div className={classes.alignCenter}>
      <div className={classes.spacer} />

      <div className={classes.mainText}>
        Du&nbsp;document&nbsp;à&nbsp;la&nbsp;connaissance partageable&nbsp;et&nbsp;actionnable
      </div>
      <div className={classes.spacer} />

      <div className={classes.videoContainer}>
        <YoutubeVideo src="https://www.youtube.com/embed/NdyfNP48MCc" />
      </div>

      <div className={classes.spacer} />
      <IconEntries />
    </div>

  );
}
