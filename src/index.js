import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import {makeStyles, createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';

import {BrowserRouter} from 'react-router-dom';
import NavHeader from './components/NavHeader';
import Footer from './components/Footer';
import MaxWidthCentered from './components/MaxWidthCentered';
import Config from './Config';
import {Switch, Route} from 'react-router-dom';
import Home from './pages/Home';
import Concept from './pages/Concept';
import Support from './pages/Support';
import Contact from './pages/Contact';
import Credits from './pages/Credits';
import Roadmap from './pages/Roadmap';
import MentionsLegales from './pages/MentionsLegales';
import Suivi from './pages/casusages/Suivi';
import Agregation from './pages/casusages/Agregation';
import Editorialisation from './pages/casusages/Editorialisation';
import Documentation from './pages/casusages/Documentation';
import ImportedGraphics from './ImportedGraphics';
import useMediaQueries from './utilities/useResponsive';
import ScrollToTop from './utilities/ScrollToTop';
import clsx from 'clsx';

const theme = createMuiTheme({
  typography: {
    fontFamily: ['Lato', 'Segoe UI Regular', 'Segoe UI', 'Roboto', '"Helvetica Neue"', 'Arial', 'sans-serif'].join(','),
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 700,
      lg: 920,
      xl: 1080,
    },
  },
});

const useStyles = makeStyles((theme) => ({
  indexRoot: {
    overflow: 'hidden',
    fontSize: 'calc(10px + 4vmin)',
    height: '100%',
    backgroundColor: Config.colors.whiteGrey,
    color: Config.colors.dark,
    display: 'flex',
    flexDirection: 'column',
  },
  indexDesktop: {
    display: 'flex',
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row'
  },
  indexMobile: {
    display: 'flex',
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'column'
  },
  fullMaxHeight: {
    minHeight: `calc(100vh - ${Config.footer.height}px - 1px)`,
    // paddingBottom: 15,
  },
  indexContent: {
    flex: 1,
    margin: 'auto',
    marginTop: Config.navbar.height,
    padding: Config.logoTop,
    [theme.breakpoints.down(Config.theme.breakpoint)]: {
      padding: '10px',
    },
    paddingTop: '0px',
    display: 'flex',
    // flex: '1 0 auto' /* Prevent Chrome, Opera, and Safari from letting these items shrink to smaller than min size. */,
    zIndex: 99,
    backgroundColor: Config.colors.white
  },
  relativeParent: {
    position: 'relative'
  },

  absoluteBackground: {
    position: 'fixed',
    background: `linear-gradient(0.25turn, ${Config.colors.weeverGreyOpacity},${Config.colors.whiteOpacity}, ${Config.colors.whiteOpacity},${Config.colors.whiteOpacity},${Config.colors.weeverGreyOpacity}),url(${ImportedGraphics.home_back_small})`,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    height: '100%',
    width: "100%",
    left: "0px",
    top: "0px",
  },
  whiteOpacityBackground: {
    backgroundColor: Config.colors.whiteOpacity  
  }
}));

function App() {
  const classes = useStyles();
  const {isDesktop} = useMediaQueries();

  const inHomeDesktop = true // isDesktop && location.pathname === "/";
  return (
    <MuiThemeProvider theme={theme}>
      <ScrollToTop>
        <div className={clsx(classes.indexRoot, classes.relativeParent)}>
          <div className={clsx(classes.fullMaxHeight, isDesktop ? classes.indexDesktop : classes.indexMobile)}>
            <NavHeader isDesktop={isDesktop} />

            <MaxWidthCentered size={'md'}>
              <div className={clsx(classes.indexContent, inHomeDesktop && classes.whiteOpacityBackground)}>
                <Switch>
                  <Route path="/concept" component={Concept} />
                  <Route path="/support" component={Support} />
                  <Route path="/contact" component={Contact} />

                  <Route path="/suivi" component={Suivi} />
                  <Route path="/agregation" component={Agregation} />
                  <Route path="/editorialisation" component={Editorialisation} />
                  <Route path="/documentation" component={Documentation} />

                  <Route path="/credits" component={Credits} />
                  <Route path="/roadmap" component={Roadmap} />
                  <Route path="/mentionslegales" component={MentionsLegales} />

                  <Route component={Home} />
                </Switch>
              </div>
            </MaxWidthCentered>
          </div>
          <Footer />
          {inHomeDesktop &&
            <div className={classes.absoluteBackground} />
          }
        </div>
      </ScrollToTop>
    </MuiThemeProvider>
  );
}

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

