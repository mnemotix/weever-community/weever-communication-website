const Config = {
  colors: {
    darkBlue: '#2a3c46',
    dark: '#000000',
    white: 'white',
    whiteGrey: "rgba(235, 235, 235, 0.2);",
    whiteOpacity: "rgba(255, 255, 255, 0.7);",
    grey: '#E6E6E6',
    green: '#A6D865',
    blue: '#1C97C3',
    weeverGrey: "#a8a8a8",
    whiteOpacity: 'rgba(255, 255, 255, 0.3)',
    weeverGreyOpacity: "rgba(168, 168, 168, 0.3)"
  },
  footer: {
    logoWidth: 350,
    height: 65
  },
  navbar: {
    height: 115,
    logoMaxHeight: 85,
  },
  logoTop: 20,
  theme: {breakpoint: 'sm'},
};
export default Config;
