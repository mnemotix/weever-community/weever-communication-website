# weever communication website (reactjs version)


## Installation


```
yarn install (utiliser 'npm install --force' si il y a une erreur de dependances à cause de react-waypoint)
```


renommer ".env.default" en ".env"

APP_PORT=80
NODE_ENV=dev
SKIP_PREFLIGHT_CHECK=true 


```
yarn start
```
si vous avez "listening on port undefined" alors le .env est mal declaré
> l'api demarre sur localhost:80 ( le port est defini dans .env )
 


Pour mettre à jour la version de production du front qui ce trouvera dans /build

```
yarn run build
```
 


Pour deployer une nouvelle version sur la prod du

```
yarn version patch
yarn release
```

Vérifier dans CI/CD que le build à fini 

https://gitlab.com/mnemotix/weever-community/weever-communication-website/-/pipelines

utiliser le numéro de build dans rancher  ex 1.0.4
https://rancher1.prod.mnemotix.com:59999/env/1a19/apps/stacks/1st250/services/1s2194/containers


## Rajout d'image

Pour rajouter des images sur reactjs il y a une méthode, il ne faut pas mettre le lien direct comme cela `<img src="assets/images/ligne-projet.png"></img>`  mais 


1) 
mettre les fichiers dans /public/images ( noms en minuscule sans char spéciaux  car on a déja eu des déconvenues, faire gaffe sur macos quand tu renommes un fichier en changeant juste la casse ca bug, ex WeeverEvent -> weeverevent, je rajoute un lettre au pif puis je l'enleve pour qu'il y a un vrai changement pour l’os )
si possible compresser les images, l'idéal est de garder une bonne résolution mais niveau de compression 80/100 ( cela donne un bien meilleur rendu que petite réso et compression 100/100), sur MacOs j'utilise ImagOptim réglé à 80% en qualité. j'ai crée un sous dossier /public/images/pages pour mettre les images et gif relatives au conteu 

what-skos.gif
weever-event.gif
editorialisation-site-drupal.png
lafayette-rebond.jpg
weever-event.png
ligne-projet.png
image14.png    // mmmm pas foufou du tout ce nom faudrait le changer




2) aller dans le fichier `src/ImportedGraphics.js`
importer les sources,  comme nom d'import on peut mettre ce que l'on veut ( whatSkos , editDrupal ) tant que c'est du camelCase

```
	import whatSkos from '../public/images/pages/what-skos.gif';
	import editDrupal from '../public/images/pages/editorialisation-site-drupal.png';
	import lafayetteRebond from '../public/images/pages/lafayette-rebond.jpg';
	import weeverEvent from '../public/images/pages/weever-event.png';
	import weeverEventGif from '../public/images/pages/weever-event.gif';
	import ligneProjet from '../public/images/pages/ligne-projet.png';
	import workflowLuma from '../public/images/pages/workflow-luma.png';
```

rajouter les noms d'import dans ImportedGraphics

```const ImportedGraphics = {  whatSkos, editDrupal , lafayetteRebond,weeverEvent, weeverEventGif ,ligneProjet ,image14  ... }```


3) reste plus qu'a utiliser ca 
 dans ton source , exemple dans `src/pages/casusages/Suivi.js`

```import ImportedGraphics from '../../ImportedGraphics';```

 dans html 
			
```<img alt="Weever" src={ImportedGraphics.logo} className={classes.logo} />```

ou classes de style

```copyright: { 	background: `url(${ImportedGraphics.footer}) no-repeat 100% 0%`	  }```


## Rajout de classes de style

pour rajouter un style global utilisé dans plusieurs fichiers il faut le rajouter ici  : `src/globalStyles.js`

exemple pour ce style `style={{fontFamily: 'LatoBold, sans-serif',fontWeight: 'bold',	fontSize: 'calc(15px + 1vmin)'}}` qui est utilisé sur tout les titres des contenus de pages, on le rajout sous le nom `latoBold15`


```
const globalStyles = makeStyles((theme) => ({  
	latoBold15 : {
		fontFamily: 'LatoBold, sans-serif',
		fontWeight: 'bold',
		fontSize: 'calc(15px + 1vmin)',
	},...
```

on va l'utiliser comme suit : 

itinialement on a ca dans `src/pages/Concept.js` : 

```
			import React from 'react';
			import PageContainer from '../components/PageContainer';

			export default function Concept(props) {

				return (
					<PageContainer title="Qu'est-ce que Weever ?">
						<div>
							<div
								style={{
									fontFamily: 'LatoBold, sans-serif',
									fontWeight: 'bold',
									fontSize: 'calc(15px + 1vmin)',
								}}
							>
```


Après modification dans `src/pages/Concept.js` : 

````
	import React from 'react';
	import PageContainer from '../components/PageContainer';
	import globalStyles from '../globalStyles'; // on import le fichier globalStyles

	export default function Concept(props) {
		const gS = globalStyles();  // on instancie les styles globaux pour ce composant/fonction/fichier

		return (
			<PageContainer title="Qu'est-ce que Weever ?">
				<div>
					<div className={gS.latoBold15} >   // et on l'utilise finalement avec className={gS.latoBold15}
````