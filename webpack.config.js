/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const env = require('env-var');

let isDev = process.env.NODE_ENV !== 'production';
let isProd = !isDev;
let isCI = !!process.env.CI;
console.log({
  isDev,
  isCI,
});

/** mode **/
let mode = isDev ? 'development' : 'production';

/** devtool **/
let devtool = isDev ? 'cheap-source-map' : false;

/** entry **/

/** entry **/
let entry = [
  ...(isDev && env.get('HOT_RELOAD_DISABLED').asBool() !== true ? ['webpack-hot-middleware/client?reload=true'] : []),
  path.join(__dirname, 'src', 'index.js'),
];

/** output **/
let output = {
  path: path.resolve(__dirname, 'dist'),
  filename: isDev ? '[name].js' : '[name].[hash].js',
  chunkFilename: isDev ? '[name].js' : '[name].[hash].js',
  publicPath: '/',
};

console.log('add hot module', isDev && env.get('HOT_RELOAD_DISABLED').asBool() !== true);

/** plugins **/
let plugins = [
  ...(!isCI ? [new webpack.ProgressPlugin()] : []),
  new HtmlWebpackPlugin({
    template: path.resolve(__dirname, 'src', 'index.html'),
    inject: 'body',
    filename: 'index.html',
  }),
  ...(isDev && env.get('HOT_RELOAD_DISABLED').asBool() !== true ? [new webpack.HotModuleReplacementPlugin()] : []),
  ...(isProd
    ? [
        new MiniCssExtractPlugin({
          filename: '[name].[hash].css',
          chunkFilename: '[id].[hash].css',
        }),
        new CompressionPlugin({
          filename: '[path][base].gz',
          algorithm: 'gzip',
          test: /\.js$|\.css$|\.html$/,
          threshold: 10240,
          minRatio: 0.8,
        }),
        new CompressionPlugin({
          filename: '[path][base].br',
          algorithm: 'brotliCompress',
          test: /\.(js|css|html|svg)$/,
          compressionOptions: {
            level: 11,
          },
          threshold: 10240,
          minRatio: 0.8,
        }),
       /* new FaviconsWebpackPlugin({
          logo: path.resolve(__dirname, 'public', 'favicon.png'),
          cache: true,          
        }),*/
      ]
    : [
       
      ]),
  new webpack.NoEmitOnErrorsPlugin(),
];

/** optimization **/
let optimization = {
  splitChunks: {
    cacheGroups: {
      defaultVendors: {
        test: /[\\/]node_modules[\\/]/,
        name: 'vendors',
        chunks: 'all',
      },
    },
  },
};

if (isProd) {
  optimization.minimizer = [
    new TerserPlugin({
      extractComments: true,
      sourceMap: true,
    }),
    new OptimizeCSSAssetsPlugin({}),
  ];
}

let devServer = {
  historyApiFallback: true,
  publicPath: '/',
  compress: true,
  //port: 9000,
};

/** modules **/
let webpackModule = {
  rules: [
    {
      test: /\.tsx?$/,
      use: [
        {
          loader: 'babel-loader',
        },
      ],
      exclude: /node_modules/,
    },
    {
      test: /\.m?js$/,
      exclude: /node_modules/,
      use: [
        {
          loader: 'babel-loader',
          options: {
            rootMode: 'upward',
          },
        },
      ],
    },
    {
      /*
       * Also, CSS dependencies in node_modules are not used by this project
       *
       */
      test: /\.css$/,
      use: [
        // style-loader
        { loader: isDev ? 'style-loader' : MiniCssExtractPlugin.loader },
        // css-loader
        {
          loader: 'css-loader',
          options: {
            modules: true,
          },
        },
      ],
    },
    {
      test: /\.(woff|woff2|eot|ttf)$/,
      use: [
        {
          loader: 'url-loader',
          options: {
            limit: 4096,
          },
        },
      ],
    },
    {
      test: /\.svg$/,
      use: ['@svgr/webpack', 'url-loader'],
    },
    {
      test: /\.(jpe?g|png|gif|ico)$/i,
      use: {
        loader: 'file-loader',
        options: {
          name: '[path][name].[hash].[ext]',
        },
      },
    },
    {
      test: /\.md/,
      use: 'raw-loader',
    },
  ],
};

const webpackConfig = {
  mode,
  devtool,
  entry,
  output,
  plugins,
  optimization,
  module: webpackModule,
  devServer,
  resolve: {
    extensions: [
      '.js',
      '.json' /* needed because some dependencies use { import './Package' } expecting to resolve Package.json */,
      '.css' /* for libraries shipping ES6 module to work */,
    ],
    /**
     * Aliases are used to enable "yarn link" to other local libraries.
     */
    alias: {
      react: require.resolve('react'),
    },
  },
  stats: {
    excludeAssets: [/favicon-plugin-icons-/],
    colors: true,
    hash: false,
    timings: true,
    chunks: false,
    chunkModules: false,
    modules: false,
  },
};

module.exports = webpackConfig;
